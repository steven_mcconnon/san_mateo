//import maps
import {Polyline} from 'google-maps-react'

let LinkedList = require('linked-list')

export class Vehicle {
	
	constructor(google) {
		this.polyLine = []
		this.bounds = null
		this.vehicleLL = new LinkedList()
		this.rawVehicleData = []
		this.google = null
	}
	
	setGoogle(google) {
		this.google = google
		this.bounds = new this.google.maps.LatLngBounds()
	}
	
	setRawVehicleData(data) {
		this.rawVehicleData = data
		
		if(this.rawVehicleData.devices) {
			for(let item of this.rawVehicleData.devices['dd7295fa-6c65-484d-b38d-30df3bc31c0c']) {
				//add item to linked list
				let temp = new LinkedList.Item()
				temp.data = item
				this.vehicleLL.append(temp)
				
				//add item to polyline
				this.polyLine.push({lat: item.lat, lng: item.long})
				
				//extend map bounds
				this.bounds.extend({lat: item.lat, lng: item.long})
			}
		}
						
	}

	
}