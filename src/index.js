import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/store'
import './css/index.css'
import App from './App'


const initialState = {
	VehicleReducer: {
		rawVehicleData: [],
		currentFrame: null,
		signalGraphData: [],
		speedGraphData: [],
		accelerationGraphData: [],
		sliderValue: 0
	}
}

const store = configureStore(initialState)

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)
