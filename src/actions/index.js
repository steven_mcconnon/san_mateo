import axios from 'axios'
import moment from 'moment'
import { toast } from 'react-toastify'

export const fetchDataReceived = (payload) => dispatch => {
	dispatch({
		type: 'FETCH_DATA_RECEIVED',
		payload: payload
	})
}

export const fetchData = (deviceId, startDate, endDate) => {
 	return (dispatch) => {
	 	let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImRqYXlAZm9yZC5jb20iLCJzeXN0ZW1Sb2xlIjoic3lzdGVtVXNlciIsImlhdCI6MTU0MTAwNzAzMiwiaXNzIjoiaHR0cDovL3dlYi1zZXJ2ZXJzLWRldi0xNDI1MzI1MDI4LnVzLXdlc3QtMi5lbGIuYW1hem9uYXdzLmNvbSIsInN1YiI6ImUyMmE2MjlkLWRlYTAtNDc0Yi04YzY5LTFlODQwYmZkMzRmYSIsImp0aSI6IjY0Nzk2YWIwLTlhYTItNGY3Ny04OTk4LWI1MzMzYzhlMmI5OCJ9.aIGEX_qigixaA17dcO0KNJay-R_704FDaugfkIAeVLA'
	 	var config = {
		    headers: {'authorization': "bearer " + token}
		}
		
		return axios.get( 
			'https://alpha.skylo.io/api/devices/history/ids/' + deviceId + '?since=' + new moment(startDate).toISOString() +  '&until=' + new moment(endDate).toISOString(),
			config
		).then((response) => {
			dispatch(fetchDataReceived(response.data))
		}).catch((error) => {
			toast.error('houston?', error)
		})
	 	
	};
}

export const setCurrentFrame = (frame) => dispatch => {
	//frame input is a LinkedListItem
	dispatch({
		type: 'CURRENT_FRAME_UPDATED',
		currentFrame: frame
	})
}

export const updateSliderValue = (direction) => dispatch => {
	//frame input is a LinkedListItem
	dispatch({
		type: 'SLIDER_UPDATED',
		direction: direction,
	})
}

export const getSignalGraphData = (direction) => dispatch => {
	// input is an array of coordinates
	dispatch({
		type: 'GET_SIGNAL_GRAPH_DATA',
		direction: direction
	})
}

export const getSpeedGraphData = (direction) => dispatch => {
	// input is an array of coordinates
	dispatch({
		type: 'GET_SPEED_GRAPH_DATA',
		direction: direction
	})
}

export const getAccelerationGraphData = (direction) => dispatch => {
	// input is an array of coordinates
	dispatch({
		type: 'GET_ACCELERATION_GRAPH_DATA',
		direction: direction
	})
}


