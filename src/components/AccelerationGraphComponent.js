import React, { Component } from 'react'

//import css
import '../App.css'

//import graphs
import { LineChart } from 'react-easy-chart'

class AccelerationGraphComponent extends Component {

	render() {			
		return (
			<div className="dashboardContainer">
				Acceleration
				<LineChart
					axes
					xType={'text'}
					yDomainRange={[-5, 5]}
					width={500}
					height={120}
					margin={{top: 10, right: 8, bottom: 23, left: 30}}
					lineColors={['green']}
					axisLabels={{x: 'Time', y: 'Acceleration'}}
					data={[this.props.accelerationGraphData?this.props.accelerationGraphData:[]]}
				/>
			</div>
		)
	}
	
}

export default AccelerationGraphComponent