import React, { Component } from 'react'

//import css
import '../App.css'

//dates
import DatePicker from 'react-datepicker'


class DatesContainer extends Component {

	render() {		
		return (
			<div className="dashboardContainer" id="dateContainer">							
				<div id="leftDateContainer">
					<label htmlFor="startDate">From:</label><br/>
					<DatePicker
					    selected={this.props.startDateTime}
					    selectsStart
					    startDate={this.props.startDate}
					    endDate={this.props.endDateTime}
					    dateFormat="h:mm:ss a dd/MM/yyyy"
						showTimeSelect
					    timeFormat="h:mm a"
					    timeIntervals={1}
					    timeCaption="time"
					    onChange={this.props.handleChangeStart}
					    popperModifiers={{
							preventOverflow: {
								enabled: true,
								escapeWithReference: false
							}
						}}
						placeholderText="Select a Start Date"
					    autoComplete="off"
					    id={'startDate'}
					/>
				</div>
				
				<div id="rightDateContainer">
					<label htmlFor="endDate">To:</label><br/>
					<DatePicker
					    selected={this.props.endDateTime}
					    selectsEnd
					    startDate={this.props.startDateTime}
					    endDate={this.props.endDateTime}
					    onChange={this.props.handleChangeEnd}
					    dateFormat="h:mm:ss a dd/MM/yyyy"
						showTimeSelect
					    timeFormat="h:mm a"
					    timeIntervals={1}
					    timeCaption="time"
					    popperModifiers={{
							preventOverflow: {
								enabled: true,
								escapeWithReference: false
							}
						}}
						placeholderText="Select an End Date"
						autoComplete="off"
					    id={'endDate'}
					/>
					
					<input type="button" value="GO" id="goButton" onClick={this.props.getVehicleData}/>
				</div>							
			</div>
		)
	}
}

export default DatesContainer


