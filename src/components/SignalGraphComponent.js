import React, { Component } from 'react'

//import css
import '../App.css'

//import graphs
import { LineChart } from 'react-easy-chart'


class SignalGraphComponent extends Component {

	render() {			
		return (
			<div className="dashboardContainer">
				Signal
				<LineChart
					axes
					xType={'text'}
					yDomainRange={[0, 4]}
					width={500}
					height={120}
					margin={{top: 10, right: 8, bottom: 23, left: 30}}
					lineColors={['green']}
					axisLabels={{x: 'Time', y: 'Signal Strength'}}
					data={[this.props.signalGraphData?this.props.signalGraphData:[]]}
				/>
			</div>
		)
	}
}

export default SignalGraphComponent


