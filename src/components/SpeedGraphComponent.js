import React, { Component } from 'react'

//import css
import '../App.css'

//import graphs
import { LineChart } from 'react-easy-chart'

class SpeedGraphComponent extends Component {

	render() {			
		return (
			<div className="dashboardContainer">
				Speed
				<LineChart
					axes
					xType={'text'}
					yDomainRange={[0, 80]}
					width={500}
					height={120}
					margin={{top: 10, right: 8, bottom: 23, left: 30}}
					lineColors={['green']}
					axisLabels={{x: 'Time', y: 'Speed'}}
					data={[this.props.speedGraphData?this.props.speedGraphData:[]]}
				/>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	currentFrame: state.VehicleReducer.currentFrame,
	speedGraphData: state.VehicleReducer.speedGraphData
})

export default SpeedGraphComponent