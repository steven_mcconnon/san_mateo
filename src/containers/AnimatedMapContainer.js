import React, { Component } from 'react'
import { connect } from 'react-redux'

//import css
import '../App.css'

//import maps
import {GoogleApiWrapper, Map, Polyline, Marker} from 'google-maps-react'

let LinkedList = require('linked-list')

class AnimatedMapContainer extends Component {
	
	constructor() {
		super()
		
		this.state = {
			startDateTime: null,
			endDateTime: null,
			sliderLevel: 50,
			vehicleData: {},
			vehicleLL: new LinkedList()
		}		
	}	

	render() {		
		
		if(this.props.google && !this.props.vehicle.google) {
			this.props.vehicle.setGoogle(this.props.google)
		}
		
		return (
			<div id="leftInnerContainer">
				<div className="dashboardContainer">
					<div style={{height: '375px', padding: '0px'}}>
						<Map google={this.props.google}
					        style={{height: "calc(100% - 20px)", width: "calc(100% - 20px)", overflow: 'hidden', bottom: '0px', overflow: 'auto'}}
					        className={'map'}
					        zoom={9}
					        bounds={this.props.vehicle.polyLine.length>0?this.props.vehicle.bounds:null}
							>
					        <Polyline
					          path={this.props.vehicle.polyLine.length > 1?this.props.vehicle.polyLine:[]}
					          strokeColor="#0000FF"
					          strokeOpacity={0.8}
					          strokeWeight={2}
					        />
							<Marker
								title={'This tracks the vehicle progress.'}
								name={'Vehicle'}
								position={this.props.currentFrame?{lat: this.props.currentFrame.data.lat, lng: this.props.currentFrame.data.long}:{}} 
							/>
							</Map>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	vehicleData: state.VehicleReducer.data,
	currentFrame: state.VehicleReducer.currentFrame
})

const mapDispatchToProps = dispatch => ({
	//fetchData: (s, e) => dispatch(fetchData(s, e))
})

export default GoogleApiWrapper({
  apiKey: 'AIzaSyDQBwF_FUbfnsM4giUlvLz_Z8B7IeogGYo'
})(connect(mapStateToProps, mapDispatchToProps)(AnimatedMapContainer))


