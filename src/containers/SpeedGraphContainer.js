import React, { Component } from 'react'
import { connect } from 'react-redux'

//import compoent
import SpeedGraphComponent from '../components/SpeedGraphComponent';

class SpeedGraphContainer extends Component {	

	render() {			
		return (
			<SpeedGraphComponent speedGraphData={this.props.speedGraphData}/>
		)
	}
}

const mapStateToProps = state => ({
	currentFrame: state.VehicleReducer.currentFrame,
	speedGraphData: state.VehicleReducer.speedGraphData
})

export default connect(mapStateToProps)(SpeedGraphContainer)