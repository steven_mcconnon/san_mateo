import React, { Component } from 'react'
import { connect } from 'react-redux'

//import component
import SignalGraphComponent from '../components/SignalGraphComponent';


class SignalGraphContainer extends Component {
	
	constructor() {
		super()
	}	

	render() {			
		return (
			<SignalGraphComponent signalGraphData={this.props.signalGraphData}/>
		)
	}
}

const mapStateToProps = state => ({
	currentFrame: state.VehicleReducer.currentFrame,
	signalGraphData: state.VehicleReducer.signalGraphData
})

export default connect(mapStateToProps)(SignalGraphContainer)