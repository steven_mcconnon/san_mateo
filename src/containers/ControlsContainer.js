import React, { Component } from 'react'
import { connect } from 'react-redux'

//import css
import '../App.css'

//import icons
import { MdSlowMotionVideo, MdPlayCircleOutline, MdFastForward, MdReplay, MdPauseCircleOutline, MdFastRewind } from 'react-icons/md'

//import actions
import { setCurrentFrame, getSignalGraphData, getSpeedGraphData, getAccelerationGraphData, updateSliderValue } from '../actions'

//import dates
import moment from 'moment'

//import toast
import { toast } from 'react-toastify'

class DatesContainer extends Component {
	
	constructor() {
		super()
		
		this.state = {
			startDateTime: null,
			endDateTime: null,
		}		
	}	
	
	handleSliderChange = (e) => {
		this.setState({
			sliderLevel: e.target.value
		})
	}
	
	playButtonClicked = () => {
		this.pauseButtonClicked()
		
		if(this.props.currentFrame) {
			this.playTimer = window.setInterval(() => {
				if(!this.props.currentFrame.next){
					clearInterval() 
				}  else {
					//set current frame for marker
					this.props.setCurrentFrame(this.props.currentFrame.next)

					//add data to graphs
					this.props.getSignalGraphData('forward')
					this.props.getSpeedGraphData('forward')
					this.props.getAccelerationGraphData('forward')

					//update slider
					this.props.updateSliderValue('forward')					
				}
			}, 150)
		} else {	
			toast.error("No data to play.")
		}
	}
	
	pauseButtonClicked = () => {
		clearInterval(this.playTimer)
	}
	
	slomoButtonClicked = () => {
		this.pauseButtonClicked()
		
		if(this.props.currentFrame) {
			this.playTimer = window.setInterval(() => {
				if(!this.props.currentFrame.next){
					clearInterval() 
				} else {
					this.props.setCurrentFrame(this.props.currentFrame.next)

					//add data to graphs
					this.props.getSignalGraphData('forward')
					this.props.getSpeedGraphData('forward')
					this.props.getAccelerationGraphData('forward')

					//update slider
					this.props.updateSliderValue('forward')
				}
			}, 500)
		} else {	
			toast.error("No data to play in slow-motion.")
		}
	}
	
	ffButtonClicked = () => {
		this.pauseButtonClicked()
		
		if(this.props.currentFrame) {
			this.playTimer = window.setInterval(() => {
				if(!this.props.currentFrame.next){
					clearInterval() 
				}  else {
					this.props.setCurrentFrame(this.props.currentFrame.next)

					//add data to graphs
					this.props.getSignalGraphData('forward')
					this.props.getSpeedGraphData('forward')
					this.props.getAccelerationGraphData('forward')

					//update slider
					this.props.updateSliderValue('forward')
				}
			}, 30)
		} else {	
			toast.error("No data to fast-forward.")
		}
	}
	
	rewindButtonClicked = () => {
		this.pauseButtonClicked()
		
		if(this.props.currentFrame) {
			this.playTimer = window.setInterval(() => {
				if(!this.props.currentFrame.prev){
					clearInterval() 
				}  else {
					this.props.setCurrentFrame(this.props.currentFrame.prev)

					//add data to graphs
					this.props.getSignalGraphData('reverse')
					this.props.getSpeedGraphData('reverse')
					this.props.getAccelerationGraphData('reverse')

					//update the slider
					this.props.updateSliderValue('reverse')
				
				}
			}, 150)
		} else {	
			toast.error("No data to rewind.")
		}
	}
	
	fastRewindButtonClicked = () => {
		this.pauseButtonClicked()
		
		if(this.props.currentFrame) {
			this.playTimer = window.setInterval(() => {
				if(!this.props.currentFrame.prev){
					clearInterval() 
				} else {
					this.props.setCurrentFrame(this.props.currentFrame.prev)

					//add data to graphs
					this.props.getSignalGraphData('reverse')
					this.props.getSpeedGraphData('reverse')
					this.props.getAccelerationGraphData('reverse')

					//update slider
					this.props.updateSliderValue('reverse')
				}
			}, 30)
		} else {	
			toast.error("No data to fast-rewind.")
		}
	}
	
	
	render() {	
		//set the max slider val
		let maxSliderVal = 100
		if(this.props.rawVehicleData.devices)	
			maxSliderVal = this.props.rawVehicleData.devices[Object.keys(this.props.rawVehicleData.devices)[0]].length

		//set the time for the slider
		let time = ""
		if(this.props.currentFrame)
			time = new moment(this.props.currentFrame.data.eventTimestamp).format("hh:mm:ss")
		
			
		return (
			<div className="controlsContainer">
				<div className="slidecontainer">
					<input type="range" min={1} max={maxSliderVal} value={this.props.sliderValue} className="slider" name="mySlider" id="myRange" onChange={this.handleSliderChange}/>
					<div className="sliderText">
						<div className="sliderTextCenter">
							{time}
						</div>
					</div>
				</div>
				<div className="controls"> 
					<MdFastRewind size='3em' className='controlButton' id="fastRewindButton" onClick={this.fastRewindButtonClicked} data-tip="Fast Rewind" /> 
					<MdReplay size='3em' className='controlButton' id="rewindButton" onClick={this.rewindButtonClicked} data-tip="Rewind" /> 
					<MdPauseCircleOutline size='3em' className='controlButton' id="pauseButton" onClick={this.pauseButtonClicked} data-tip="Pause" /> 
					<MdSlowMotionVideo size='3em' className='controlButton' id="slomoButton" onClick={this.slomoButtonClicked} data-tip="Play 1/2 Speed" /> 
					<MdPlayCircleOutline size='3em' className='controlButton' id="playButton" onClick={this.playButtonClicked} data-tip="Play" /> 
					<MdFastForward size='3em' className='controlButton' id="ffButton" onClick={this.ffButtonClicked} data-tip="Fast Forward" /> 
				</div>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	vehicleData: state.VehicleReducer.data,
	currentFrame: state.VehicleReducer.currentFrame,
	signalGraphData: state.VehicleReducer.signalGraphData,
	sliderValue: state.VehicleReducer.sliderValue,
	rawVehicleData: state.VehicleReducer.rawVehicleData
})
//getSignalGraphData
const mapDispatchToProps = dispatch => ({
	setCurrentFrame: (f) => dispatch(setCurrentFrame(f)),
	getSignalGraphData: (d) => dispatch(getSignalGraphData(d)),
	getSpeedGraphData: (d) => dispatch(getSpeedGraphData(d)),
	getAccelerationGraphData: (d) => dispatch(getAccelerationGraphData(d)),
	updateSliderValue: (d) => dispatch(updateSliderValue(d))
})

export default connect(mapStateToProps, mapDispatchToProps)(DatesContainer)


