import React, { Component } from 'react'
import { connect } from 'react-redux'

//dates
import moment from 'moment'

//import actions
import { fetchData } from '../actions'

//import component
import DatesComponent from '../components/DatesComponent';

class DatesContainer extends Component {
	
	constructor() {
		super()
		
		this.state = {
			startDateTime: null,
			endDateTime: null,
		}		
	}	
	
	handleChangeStart = (date) => {

		this.setState({
			startDateTime: date
		})
		
	}
	
	handleChangeEnd = (date) => {
		this.setState({
			endDateTime: date
		})
	}
	
	getVehicleData = () => {
		this.props.fetchData('dd7295fa-6c65-484d-b38d-30df3bc31c0c', this.state.startDateTime, this.state.endDateTime)
	}

	render() {		
		return (
			<DatesComponent 
				vehicleData={this.vehicleData} 
				getVehicleData={this.getVehicleData}
				handleChangeEnd={this.handleChangeEnd} 
				handleChangeStart={this.handleChangeStart}
				startDateTime={this.state.startDateTime}
				endDateTime={this.state.endDateTime}
			/>
		)
	}
}

const mapStateToProps = state => ({
	vehicleData: state.VehicleReducer.data
})

const mapDispatchToProps = dispatch => ({
	fetchData: (s, e) => dispatch(fetchData(s, e))
})

export default connect(mapStateToProps, mapDispatchToProps)(DatesContainer)


