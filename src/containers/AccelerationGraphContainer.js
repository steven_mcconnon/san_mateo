import React, { Component } from 'react'
import { connect } from 'react-redux'

//import css
import '../App.css'

//import component
import AccelerationGraphComponent from '../components/AccelerationGraphComponent'


class AccelerationGraphContainer extends Component {

	render() {			
		return (
			<AccelerationGraphComponent accelerationGraphData={this.props.accelerationGraphData} />
		)
	}
	
}

const mapStateToProps = state => ({
	currentFrame: state.VehicleReducer.currentFrame,
	accelerationGraphData: state.VehicleReducer.accelerationGraphData
})

export default connect(mapStateToProps)(AccelerationGraphContainer)