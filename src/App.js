import React, { Component } from 'react'
import { connect } from 'react-redux'

//import toast
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

//components
import AccelerationGraphContainer from './containers/AccelerationGraphContainer'
import SpeedGraphContainer from './containers/SpeedGraphContainer'
import SignalGraphContainer from './containers/SignalGraphContainer'
import DatesContainer from './containers/DatesContainer'
import AnimatedMapContainer from './containers/AnimatedMapContainer'
import ControlsContainer from './containers/ControlsContainer'

//import css
import './App.css'
import "react-datepicker/dist/react-datepicker.css"

//tooltips
import ReactTooltip from 'react-tooltip'

//import action
import { fetchData, setCurrentFrame } from './actions'

//helper class
import { Vehicle } from './helpers/classes/Vehicle'


class App extends Component {
	
	constructor() {
		super()
		
		this.state = {
			sliderLevel: 50,
			vehicleData: {},	
		}		
		
		this.v = new Vehicle()
	}
		
	render() {
				
		//if new data
		if(this.props.vehicleData && this.props.vehicleData !== this.v.rawVehicleData) {
			this.v.setRawVehicleData(this.props.vehicleData)
			this.props.setCurrentFrame(this.v.vehicleLL.head)
		}
					
		return (
			<div id="app">
				<div className="leftPanel">
					<h1>Vehicle Statistics</h1>
					<AnimatedMapContainer vehicle={this.v} />
					
					<div className="leftPanel">
						<ControlsContainer vehicle={this.v} />
					</div>
				</div>
				
				<div id="rightPanel">
					<div id="rightInnerContainer">
						<DatesContainer />
						<SignalGraphContainer vehicle={this.v} />
						<SpeedGraphContainer />
						<AccelerationGraphContainer />
					</div>
				</div>
				<ReactTooltip />
				<ToastContainer />
			</div>
		)
	}
}

const mapStateToProps = state => ({
	vehicleData: state.VehicleReducer.rawVehicleData,
	currentFrame: state.VehicleReducer.currentFrame
})

const mapDispatchToProps = dispatch => ({
	fetchData: (s, e) => dispatch(fetchData(s, e)),
	setCurrentFrame: (f) => dispatch(setCurrentFrame(f))
})

export default connect(mapStateToProps, mapDispatchToProps)(App)


