import moment from 'moment'

export default (state = {}, action) => {
	switch (action.type) {
		case 'FETCH_DATA_RECEIVED': {
			return {
				...state,
				rawVehicleData: action.payload
			}
		}
		case 'CURRENT_FRAME_UPDATED': {
			return {
				...state,
				currentFrame: action.currentFrame
			}
		} 
		case 'SLIDER_UPDATED': {
			let val = 0

			if(action.direction === 'forward') 
				val = state.sliderValue + 1
			else	
				val = state.sliderValue - 1

			return {
				...state,
				sliderValue: val
			}
		}
		case 'GET_SIGNAL_GRAPH_DATA': {
			let newData = state.currentFrame?state.signalGraphData:[]

			if(action.direction === 'forward') {
				if(state.currentFrame) {
					if(newData.length > 10)
						newData.shift()

					let time = new moment(state.currentFrame.data.eventTimestamp).format("hh:mm:ss")
					let signal = state.currentFrame.data.rssi

					newData.push({x: time, y: signal})
				}
			} else if(action.direction === 'reverse' && newData.length > 2) {
				if(state.currentFrame) {
					//take off the last item
					newData.pop()

					//get first frame
					let firstFrame = state.currentFrame
					for(let i = 0; i < newData.length; i++) 
						firstFrame = firstFrame.prev

					if(firstFrame) {
						let time = new moment(firstFrame.data.eventTimestamp).format("hh:mm:ss")
						let speed = firstFrame.data.rssi

						newData.unshift({x: time, y: speed})
					}
				}
			} 

			return {
				...state,
				signalGraphData: newData
			}
		}
		case 'GET_SPEED_GRAPH_DATA': {
			let newData = state.currentFrame?state.speedGraphData:[]

			if(action.direction === 'forward') {
				if(state.currentFrame) {
					if(newData.length > 10)
						newData.shift()

					let time = new moment(state.currentFrame.data.eventTimestamp).format("hh:mm:ss")
					let speed = state.currentFrame.data.speed

					newData.push({x: time, y: speed})
				}
			} else if(action.direction === 'reverse') {
				if(state.currentFrame && newData.length > 2) {
					//take off the last item
					newData.pop()

					//get first frame
					let firstFrame = state.currentFrame
					for(let i = 0; i < newData.length; i++) 
						firstFrame = firstFrame.prev
					
						if(firstFrame) {
							let time = new moment(firstFrame.data.eventTimestamp).format("hh:mm:ss")
							let speed = firstFrame.data.speed

							newData.unshift({x: time, y: speed})
						}
				}
			}

			return {
				...state,
				speedGraphData: newData
			}
		}
		case 'GET_ACCELERATION_GRAPH_DATA': {
			let newData = state.currentFrame?state.accelerationGraphData:[]

			if(action.direction === 'forward') {
				if(state.currentFrame) {
					if(newData.length > 10)
						newData.shift()

					let time = new moment(state.currentFrame.data.eventTimestamp).format("hh:mm:ss")
					let end = new moment(state.currentFrame.data.eventTimestamp)
					let start = new moment(state.currentFrame.prev.data.eventTimestamp)
					let timeDiff = moment.duration(end.diff(start)).asSeconds()
					let speedDiff = state.currentFrame.data.speed - state.currentFrame.prev.data.speed
					let mphPerSecond = speedDiff/timeDiff

					newData.push({x: time, y: mphPerSecond})
				} 
			} else if(action.direction === 'reverse' && newData.length > 2) {
				if(state.currentFrame) {
					//take off the last item
					newData.pop()

					//get first frame
					let firstFrame = state.currentFrame
					for(let i = 0; i < newData.length; i++) 
						firstFrame = firstFrame.prev
					
					if(firstFrame) {
						//calculate acceleration
						let time = new moment(firstFrame.data.eventTimestamp).format("hh:mm:ss")
						let end = new moment(firstFrame.next.data.eventTimestamp)
						let start = new moment(firstFrame.data.eventTimestamp)
						let timeDiff = moment.duration(end.diff(start)).asSeconds()
						let speedDiff = firstFrame.data.speed - firstFrame.next.data.speed
						let mphPerSecond = speedDiff/timeDiff

						newData.unshift({x: time, y: mphPerSecond})
					}
				} 
			}

			return {
				...state,
				accelerationGraphData: newData
			}
		}
		default: {
			return {
				...state
			}
		}
	}
}


